@echo off

pushd %~dp0
SET SCRIPTS=%CD%
popd

call build-maven.bat

docker network create -d bridge dws-jenkins
docker build -t dws/jenkins %SCRIPTS%\..\jenkins\
docker rm -vf dws_jenkins
docker run -d -p 8888:8888 ^
           --net dws-jenkins ^
           -v /var/jenkins_home:/var/jenkins_home ^
           -v /var/run/docker.sock:/var/run/docker.sock ^
           --name dws_jenkins dws/jenkins


@echo off

pushd %~dp0
SET SCRIPTS=%CD%
popd

docker build -t dws/mvn %SCRIPTS%\..\maven\base\
docker build -t dws/mvn-front-end %SCRIPTS%\..\maven\front-end\
docker build -t dws/mvn-e2e %SCRIPTS%\..\maven\e2e\